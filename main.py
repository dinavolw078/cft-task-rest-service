from datetime import datetime, timedelta
import jwt
from flask import Flask, request

app = Flask(__name__)

# Простая база данных пользователей, которых можно авторизировать
users = {
    "user1": {"username": "user1", "password": "password1", "salary": 50000, "next_promotion_date": "2024-01-01"},
    "user2": {"username": "user2", "password": "password2", "salary": 60000, "next_promotion_date": "2023-12-15"}
}

# Секретный ключ для генерации JWT
app.config['SECRET_KEY'] = 'secret_key'


# Метод для генерации JWT
def generate_token(username):
    payload = {
        'username': username,
        'exp': datetime.utcnow() + timedelta(seconds=token_exp_time)
    }
    token = jwt.encode(payload, app.config['SECRET_KEY'], algorithm='HS256')
    return token


# Метод для проверки валидности токена
def verify_token(token):
    try:
        payload = jwt.decode(token, app.config['SECRET_KEY'], algorithms='HS256')
        if datetime.utcnow() > datetime.fromtimestamp(payload['exp']):
            print("time")
            return False
        else:
            return True
    except jwt.exceptions.DecodeError:
        print("DecodeError")
        return False
    except jwt.exceptions.ExpiredSignatureError:
        print("token expired")
        return False


# Время жизни токена (5 минут)
token_exp_time = 300


@app.route('/')
def srart():
    url = request.url
    print(url)
    return '''
    <html>
    <head>
        <title>CFT Task - REST Service</title>
    </head>
    <body>
        <h1>Welcome, please sign in!</h1>
        <form action="'''+url+'''/login?username=<?=$_POST['username']>&password<?=$_POST['password']>"   
        target="_blank">
        <p><b>Login:</b><br>
        <input type="text" size="40" name=username>
        </p>
        <p><b>Password:</b><br>
        <input type="text" size="40" name=password>
        </p>
        <button>Sign in</button>
        </form>
    </body>
    </html>'''


@app.route('/login', methods=['GET', 'POST'])
def login():
    username = request.args.get('username')
    password = request.args.get('password')
    url = request.url.removesuffix("/login?username=" + username + "&password=" + password)

    if username == '' or password == '':
        return '''
           <html>
            <head>
                <title>Log in Error</title>
            </head>
            <body>
                <h1>You used blank login or password</h1>
                <form action="/" target="'''+url+'''">
                <button>Try Again</button>
                <form>
            </body>
            </html>''', 401

    # Проверяем логин и пароль
    for user in users:
        if username == users.get(user).get("username") and password == users.get(user).get("password"):
            token = generate_token(username)
            return '''
            <html>
            <head>
                <title>Successful</title>
                <meta http-equiv="refresh" content="1; URL='''+url+"/salary?token="+token+"&username="+username+'''">
            </head>
            <body>
                <b>Your Token is:'''+token+'''<b>
            </body>
            </html>''', 200

    return '''
   <html>
    <head>
        <title>Log in Error</title>
    </head>
    <body>
        <h1>Wrong login or password</h1>
        <h1>Your input was:</h1>
        <h1>Login: '''+username+'''</h1>
        <h1>Password: '''+password+'''</h1>
        <form action="/" target="'''+url+'''">
        <button>Try Again</button>
        <form>
    </body>
    </html>''', 401


@app.route('/salary', methods=['GET'])
def get_salary():
    # Проверяем наличие токена в запросе
    token = request.args.get('token')
    if token == '':
        return '''
        <html>
        <head>
            <title>Token Error</title>
        </head>
        <body>
            <h1>You dont have a token. Please, start on the /login page.</h1>
        </body>
        </html>''', 401
    user = request.args.get('username')
    # Проверяем, что токен предоставленнный пользователем действителен
    if verify_token(token):
        return '''
            <html>
            <head>
                <title>Successful Request</title>
            </head>
            <body>
                <h1>Your salary is: '''+str(users.get(user).get("salary"))+'''</h1>
                <h1>Your next promotion date is: '''+str(users.get(user).get("next_promotion_date"))+'''</h1>
            </body>
            </html>''', 200

    return '''
        <html>
        <head>
            <title>Token Error</title>
        </head>
        <body>
            <h1>Invalid Token</h1>
        </body>
        </html>''', 401


if __name__ == '__main__':
    app.run(debug=False)
